from selenium import webdriver
import time
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome(ChromeDriverManager().install())

with open('Nomes.txt', 'r', encoding='utf-8') as contato:
    contatos = contato.readlines()

nomeGetSet = ""
texto = ""

def get_nome():
    return nomeGetSet.strip()


def enviar_promocao():

    with open('Promoção.txt', 'r', encoding='utf-8') as texto:
        promocao = texto.read()
    
    mensagem()

    clip = driver.find_element_by_css_selector("span[data-testid='clip']")
    clip.click()

    attach = driver.find_element_by_css_selector("input[type='file']")
    attach.send_keys(promocao)

    elemento = driver.find_elements_by_css_selector("span[data-testid='send']")
    teste = len(elemento)

    cond = True
    while (cond == True):
        time.sleep(1)
        if (teste == 0):
            elemento = driver.find_elements_by_css_selector("span[data-testid='send']")
            teste = len(elemento)
        else:
            cond = False

    send = driver.find_element_by_css_selector("span[data-testid='send']")
    send.click()
    
    elemento = driver.find_elements_by_xpath("//span[contains(@data-testid, 'msg-time')]")
    test_send = len(elemento)
    
    cond = True
    while (cond == True):
        time.sleep(1)
        if (test_send > 0):
            elemento = driver.find_elements_by_xpath("//span[contains(@data-testid, 'msg-time')]")
            test_send = len(elemento)
        else:
            cond = False

    print("Próximo")


def mensagem():

    with open('Mensagem.txt', 'r', encoding='utf-8') as texto:
        mensagem = texto.read()

    elemento = driver.find_elements_by_xpath("//button[contains(@class ,'_27F2N')]")
    teste = len(elemento)

    if (teste == 1):
        time.sleep(2)
        botao = driver.find_element_by_xpath("//button[contains(@class ,'_27F2N')]")
        botao.click()

    campo_mensagem = driver.find_elements_by_xpath('//div[contains(@class, "copyable-text selectable-text")]')
    campo_mensagem[1].click()
    campo_mensagem[1].send_keys(mensagem)
    campo_mensagem[1].send_keys(Keys.ENTER)


def enviar_mensagem():

    mensagem()

    elemento = driver.find_elements_by_xpath("//span[contains(@data-testid, 'msg-time')]")
    test_send = len(elemento)
    
    cond = True
    while (cond == True):
        time.sleep(1)
        if (test_send > 0):
            elemento = driver.find_elements_by_xpath("//span[contains(@data-testid, 'msg-time')]")
            test_send = len(elemento)
        else:
            cond = False

    contato_andamento = open('Envio_andamento.txt', 'a', encoding='utf-8')
    contato_andamento.write(get_nome() + "\n")
    contato_andamento.close()

    print("Próximo")


def pesquisar_campo():

    campo_pessoa = driver.find_element_by_xpath('//div[contains(@class, "copyable-text selectable-text")]')
    campo_pessoa.click()
    campo_pessoa.clear()
    campo_pessoa.send_keys(get_nome())

    time.sleep(2)

    elemento = driver.find_elements_by_xpath(f"//span[contains(text(),'{get_nome()}')]")
    teste = len(elemento)
    
    cond = True

    if (teste == 1):
        campo_pessoa.send_keys(Keys.ENTER)
    elif (teste > 1):
        repetido()
        cond = False
    else:
        cont = 0
        while (cond == True):
            time.sleep(2)
            if (teste == 0):
                if (cont <= 2):
                    elemento = driver.find_elements_by_xpath(f"//span[contains(text(),'{get_nome()}')]")
                    teste = len(elemento)
                    cont = cont + 1
                else:
                    cond = False
                    erro()
            else:
                cond = False

    contato_andamento = open('Envio_andamento.txt', 'a', encoding='utf-8')
    contato_andamento.write(get_nome() + "\n")
    contato_andamento.close()
    return cond

def lista_andamento():
    with open('Envio_andamento.txt', 'r', encoding='utf-8') as contato_atual:
        arquivo = contato_atual.readlines()
    nomes = ""
    for linha in arquivo:
        nomes = nomes + linha
    
    print("\n\n\n === Andamento da Lista ===\n" + nomes)


def sair():
    contatos.clear()


def erro():
    contato_andamento = open('Erros/Envio_erro.txt', 'a', encoding='utf-8')
    contato_andamento.write(get_nome() + "\n")
    contato_andamento.close()

    contato_andamento = open('Envio_andamento.txt', 'a', encoding='utf-8')
    contato_andamento.write(get_nome() + "\n")
    contato_andamento.close()

    print("Envio cancelado!")


def repetido():
    contato_andamento = open('Erros/Envio_repetidos.txt', 'a', encoding='utf-8')
    contato_andamento.write(get_nome() + "\n")
    contato_andamento.close()

    contato_andamento = open('Envio_andamento.txt', 'a', encoding='utf-8')
    contato_andamento.write(get_nome() + "\n")
    contato_andamento.close()

    print("Envio cancelado!")


def caso(op):
    opcoes = {
    1: enviar_mensagem,
    2: enviar_promocao
    }

    return opcoes.get(op, "Opção inválida")

opcao = input('\n\n\n -------- Para envio de mensagem digite "1" --------\n -------- Para envio de promoção digite "2" --------\n\n\n')

driver.get('https://web.whatsapp.com/')

input('\n\n\n**** Quando a página carregar dê um "ENTER" ****\n\n\n')

for pessoas in contatos:
    nomeGetSet = pessoas.strip()
    print(get_nome())
    
    if (pesquisar_campo()):
        opcao_int = int(opcao)

        output = caso(opcao_int)
        output()
        
    lista_andamento()

driver.close()
quit()
