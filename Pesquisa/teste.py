from selenium import webdriver
import time
import keyboard
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get('https://web.whatsapp.com/')

with open('Pesquisa.txt', 'r', encoding='utf-8') as contato:
    contatos = contato.readlines()

nomeGetSet = ""

def get_nome():
    return nomeGetSet

def pesquisar_campo():

    campo_pessoa = driver.find_element_by_xpath('//div[contains(@class, "copyable-text selectable-text")]')
    campo_pessoa.click()
    campo_pessoa.clear()
    campo_pessoa.send_keys(get_nome())

    time.sleep(2)

    elemento = driver.find_elements_by_xpath(f"//span[contains(text(),'{get_nome()}')]")
    teste = len(elemento)
    
    cond = True
    cont = 0

    if (teste == 1):
        campo_pessoa.send_keys(Keys.ENTER)
    elif (teste > 1):
        campo_pessoa.send_keys(Keys.ENTER)

        with open('../Mensagem.txt', 'r', encoding='utf-8') as texto:
            mensagem = texto.read()

        elemento = driver.find_elements_by_xpath(f"//span[contains(text(),'{mensagem}')]")
        teste = len(elemento)
        
        if (teste == 0):
            repetido()
            cond = False

        elemento = driver.find_element_by_xpath(f"//span[contains(@title,'{get_nome()}')]").text()
        teste = elemento.startswith(f'{get_nome()}')
        
        if (teste):
            
            teste2 = get_nome().split(' ')
            for partes in teste2:
                if(teste.endswith(partes)):
                    cont = cont + 1    
            
            if (cont > 1):
                cond = False
    else:
        cont = 0
        while (cond == True):
            time.sleep(2)
            if (teste == 0):
                if (cont <= 2):
                    elemento = driver.find_elements_by_xpath(f"//span[contains(text(),'{get_nome()}')]")
                    teste = len(elemento)
                    cont = cont + 1
                else:
                    cond = False
                    erro()
            else:
                cond = False

    contato_andamento = open('Envio_andamento.txt', 'a', encoding='utf-8')
    contato_andamento.write(get_nome() + "\n")
    contato_andamento.close()

def lista_andamento():
    with open('Pesquisa_andamento.txt', 'r', encoding='utf-8') as contato_atual:
        arquivo = contato_atual.readlines()
    nomes = ""
    for linha in arquivo:
        nomes = nomes + linha
    
    print("\n\n\n === Andamento da Lista ===\n" + nomes)

def sair():
    contatos.clear()

def erro():
    contato_novo = open('Erros/Pesquisa_erro.txt', 'a', encoding='utf-8')
    contato_novo.write(get_nome() + "\n")
    contato_novo.close()

def repetido():
    contato_novo = open('Erros/Pesquisa_repetidos.txt', 'a', encoding='utf-8')
    contato_novo.write(get_nome() + "\n")
    contato_novo.close()

input('\n\n\n**** Quando a página carregar dê um "ENTER" ****\n\n\n')

for pessoas in contatos:
    nomeGetSet = pessoas.strip()
    print(get_nome())
    pesquisar_campo()      
    lista_andamento()

driver.close()
quit()

